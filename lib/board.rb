require_relative "peg"
class Board
protected
	attr_accessor :pegs
public
  def initialize(peg_1, peg_2, peg_3, peg_4)
    if (peg_1.class != Peg ||  peg_2.class != Peg ||  peg_3.class != Peg ||  peg_4.class != Peg )
      raise ArgumentError, "A board can only be initialized with pegs."
    end
    @pegs = [peg_1, peg_2, peg_3, peg_4]
  end # initialize

  def peg_at i 
    raise ArgumentError if !(1..4).include?(i)
    @pegs[i-1]
  end 

  def print_string 
    "  -----------------\n  | #{peg_at(1).to_s} | #{peg_at(2).to_s} | #{peg_at(3).to_s} | #{peg_at(4).to_s} |\n  -----------------\n"
  end

  def eql? board_2
    @pegs == board_2.pegs
  end

  def == board_2
    @pegs == board_2.pegs
  end

  def hash
    @pegs.hash
  end


end # Board
