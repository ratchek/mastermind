class Peg
  attr_reader :value
  RANGE ||= (1..6)
  def initialize value
    raise ArgumentError unless RANGE.include? value
    @value = value
  end

  def self.values
    RANGE.to_a  
  end

  def to_s 
    @value.to_s
  end
  
  def eql? peg_2
    self.value == peg_2.value
  end

  def == peg_2
    self.value == peg_2.value
  end

  def hash 
    value.hash
  end

end # Peg
