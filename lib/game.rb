require_relative "./board"
require_relative "./response"

class Game
  GUESSES_ALLOWED = 6
  
  def initialize board
    @code_board = board
    @guesses_made = 0
  end 

  def guess guess_board
    @guesses_made += 1
    return [-1,Response.new(0,0,0,0)] unless @guesses_made <= GUESSES_ALLOWED
    return [1,Response.new(2,2,2,2)] if guess_board == @code_board
# return -1 if this was the last available guess
    return [-1,Response.new(0,0,0,0)] if @guesses_made == GUESSES_ALLOWED
    0
  end
end # Game

=begin
peg_1 = Peg.new(3)
peg_2 = Peg.new(4)
game = Game.new(Board.new(peg_1,peg_1,peg_1,peg_1))
incorrect_board = Board.new(peg_2,peg_2,peg_2,peg_2)
correct_board = Board.new(peg_1,peg_1,peg_1,peg_1)
5.times {|i| puts i.to_s + "    " + game.guess(incorrect_board)[0].to_s}
 
puts game.guess(correct_board)[0]
=end
