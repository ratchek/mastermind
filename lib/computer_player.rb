require_relative "player"

class ComputerPlayer < Player
  def initialize
    name = ["Hal 9000", "T-1000", "R2D2", "C-3PO", "Agent Smith,", "Wall-E", "Marvin"].sample
    super(name)
  end

  def generate_code
    Board.new(random_peg,random_peg,random_peg,random_peg)
  end

  private
  def random_peg
    Peg.new(Peg.values.sample)
  end
end
