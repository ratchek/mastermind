class Response
  protected
    attr_accessor :array
  public
  def initialize (a,b,c,d)
    unless((0..2).include?(a) && (0..2).include?(b) && (0..2).include?(c) && (0..2).include?(d))
      raise ArgumentError, "The only acceptable arguments are 0, 1, or 2"
    end
    @array = [a,b,c,d]
  end # initialize

  def at i 
    raise ArgumentError if !(1..4).include?(i)
    @array[i-1]
  end 

  def set (i,val) 
    raise ArgumentError, "only values that are permited are 0, 1, or 2"   unless((0..2).include?(val))
    raise ArgumentError, "only indices that are permited are 1,2,3, or 4" unless (1..4).include?(i)
    @array[i-1] = val
  end 

  def eql? response_2
    @array == response_2.array
  end

  def == response_2
    @array == response_2.array
  end

  def hash
    @array.hash
  end


end # Response
