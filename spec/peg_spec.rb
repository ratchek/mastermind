require_relative "../lib/peg"

describe Peg do
  context "initialized with 1" do
    subject { Peg.new(1) }
    it {is_expected.to have_attributes(value: 1) } 
    
    describe ".to_s" do
      it "is expected to return '1' " do
        expect(subject.to_s).to eq("1")
      end
    end # .print
  end # initialized with 1

  context "initialized with 2" do
    subject { Peg.new(2) }
    it {is_expected.to have_attributes(value: 2) } 
    
    describe ".to_s" do
      it "is expected to return '2' " do
        expect(subject.to_s).to eq("2")
      end
    end # .print
  end # initialized with 2


  context "initialized with 0" do
    it "raises an ArgumentError" do
      expect{Peg.new(0)}.to raise_error(ArgumentError)
    end
  end

  context "initialized with 'a'" do
    it "raises an ArgumentError" do
      expect{Peg.new('a')}.to raise_error(ArgumentError)
    end
  end

  context "initialized with the string '1'" do
    it "raises an ArgumentError" do
      expect{Peg.new('1')}.to raise_error(ArgumentError)
    end
  end
  
  describe ".eql" do
    context "comparing two different Pegs with equal values" do
      it "returns true" do
        peg_1 = Peg.new(2)
        peg_2 = Peg.new(2)
        expect(peg_1).to eql(peg_2)
      end
    end

    context "comparing two different Pegs with different values" do
      it "returns false" do
        peg_1 = Peg.new(2)
        peg_2 = Peg.new(3)
        expect(peg_1).not_to eql(peg_2)
      end
    end
  end # .eql?

  describe ".==" do
    context "comparing two different Pegs with equal values" do
      it "returns true" do
        peg_1 = Peg.new(2)
        peg_2 = Peg.new(2)
        expect(peg_1==peg_2).to be true
      end
    end

    context "comparing two different Pegs with different values" do
      it "returns false" do
        peg_1 = Peg.new(2)
        peg_2 = Peg.new(3)
        expect(peg_1==peg_2).to be false
      end
    end
  end # .==


  describe ".hash" do
    context "comparing hashes of two different Pegs with equal values" do
      it "returns true" do
        peg_1 = Peg.new(2)
        peg_2 = Peg.new(2)
        expect(peg_1.hash == peg_2.hash).to be true
      end
    end

    context "comparing hashes of two different Pegs with different values" do
      it "returns false" do
        peg_1 = Peg.new(2)
        peg_2 = Peg.new(3)
        expect(peg_1.hash == peg_2.hash).to be false
      end
    end
  end # .hash

  describe "#values" do
    it "should return an array [1,2,3,4,5,6]" do
      expect(Peg.values).to eq([1,2,3,4,5,6])
    end
  end
end #Peg
