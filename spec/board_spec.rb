require_relative "../lib/board"

describe Board do
  context "when initialized with pegs 1,5,2,2" do
    board =  Board.new(Peg.new(1), Peg.new(5), Peg.new(2), Peg.new(2)) 
    describe ".peg_at " do
      context "1" do
        it "returns a peg with value 1" do
          expect(board.peg_at(1)).to eql(Peg.new(1))
        end
      end 

      context "2" do
        it "returns a peg with value 5" do
          expect(board.peg_at(2)).to eql(Peg.new(5))
        end
      end 

      context "3" do
        it "returns a peg with value 2" do
          expect(board.peg_at(3)).to eql(Peg.new(2))
        end
      end 

      context "4" do
        it "returns a peg with value 2" do
          expect(board.peg_at(4)).to eql(Peg.new(2))
        end
      end 

      context "5" do
        it "throws an ArgumentError" do
          expect{board.peg_at(5)}.to raise_error ArgumentError
        end
      end

      context "0" do
        it "throws an ArgumentError" do
          expect{board.peg_at(0)}.to raise_error ArgumentError
        end
      end

      context "'c'" do
        it "throws an ArgumentError" do
          expect{board.peg_at("c")}.to raise_error ArgumentError
        end
      end

      context "'2'" do
        it "throws an ArgumentError" do
          expect{board.peg_at("2")}.to raise_error ArgumentError
        end
      end

     end # peg_at

    describe "print_string" do
      context "when called" do
        it "returns a string for printing the board" do
          expect(board.print_string).to eql("  -----------------\n  | 1 | 5 | 2 | 2 |\n  -----------------\n") 
        end
      end
    end
  
  end # context -  initialized with pegs

  context "when initialized with 3 pegs and just a regular number (2)" do
      let(:peg)  {Peg.new(1)}
      it "returns throws an argument error" do
        expect{Board.new(peg,peg,peg,2)}.to raise_error ArgumentError
      end
  end 

  describe ".eql" do
    context "comparing two different Boards with same pegs" do
      it "returns true" do
        board_1 = Board.new(Peg.new(2),Peg.new(1),Peg.new(2),Peg.new(4))
        board_2 = Board.new(Peg.new(2),Peg.new(1),Peg.new(2),Peg.new(4))
        expect(board_1).to eql(board_2)
      end
    end

    context "comparing two different Boards with different pegs" do
      it "returns false" do
        board_1 = Board.new(Peg.new(2),Peg.new(1),Peg.new(2),Peg.new(3))
        board_2 = Board.new(Peg.new(2),Peg.new(1),Peg.new(3),Peg.new(2))
        expect(board_1).not_to eql(board_2)
      end
    end
  end # .eql?

  describe ".==" do
    context "comparing two different Boards with equal pegs" do
      it "returns true" do
        board_1 = Board.new(Peg.new(5),Peg.new(5),Peg.new(5),Peg.new(5))
        board_2 = Board.new(Peg.new(5),Peg.new(5),Peg.new(5),Peg.new(5))
        expect(board_1==board_2).to be true
      end
    end

    context "comparing two different Boards with different pegs" do
      it "returns false" do
        board_1 = Board.new(Peg.new(5),Peg.new(5),Peg.new(5),Peg.new(5))
        board_2 = Board.new(Peg.new(2),Peg.new(5),Peg.new(5),Peg.new(5))
        expect(board_1==board_2).to be false
      end
    end
  end # .==

  describe ".hash" do
    context "comparing hashes of two different Boards with equal pegs" do
      it "returns true" do
        board_1 = Board.new(Peg.new(6),Peg.new(1),Peg.new(3),Peg.new(3))
        board_2 = Board.new(Peg.new(6),Peg.new(1),Peg.new(3),Peg.new(3))
        expect(board_1.hash == board_2.hash).to be true
      end
    end

    context "comparing hashes of two different Boards with different pegs" do
      it "returns false" do
        board_1 = Board.new(Peg.new(6),Peg.new(1),Peg.new(3),Peg.new(3))
        board_2 = Board.new(Peg.new(6),Peg.new(3),Peg.new(1),Peg.new(3))
        expect(board_1.hash == board_2.hash).to be false
      end
    end
  end # .hash



end # Board
