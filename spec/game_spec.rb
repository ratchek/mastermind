require_relative "../lib/game"

describe Game do
  context "when initialized with '2,2,2,2' " do
    subject(:game) { Game.new(Board.new(Peg.new(2), Peg.new(2), Peg.new(2), Peg.new(2)))}
    
    describe ".guess" do 
      context "when guessed incorrectly on first try" do
        it "returns 0" do
          expect( game.guess(Board.new(Peg.new(2), Peg.new(3), Peg.new(2), Peg.new(2)))[0] ).to eql(0)
        end
      end  
      
      context "when guessed incorrectly on #{Game::GUESSES_ALLOWED-1}th try" do
        before do
          peg = Peg.new(4)
          @board = Board.new(peg,peg,peg,peg)
          (Game::GUESSES_ALLOWED-2).times {game.guess(@board)}
        end
        it "returns 0 as the first element of the return array" do
          expect( (game.guess(@board))[0]).to eql(0) 
        end
      end

      context "when guessed incorrectly on #{Game::GUESSES_ALLOWED}th try" do
        before do
          peg = Peg.new(4)
          @board = Board.new(peg,peg,peg,peg)
          (Game::GUESSES_ALLOWED-1).times {game.guess(@board)}
        end
        it "returns -1 as the first element of the return array" do
          expect( (game.guess(@board))[0]).to eql(-1) 
        end
        it "returns a Response filled with 0s as the second element of the return array" do
          expect( (game.guess(@board))[1]).to eql(Response.new(0,0,0,0)) 
        end
      end

      context "when guessed incorrectly on #{Game::GUESSES_ALLOWED+1}th try" do
        before do
          peg = Peg.new(4)
          @board = Board.new(peg,peg,peg,peg)
          (Game::GUESSES_ALLOWED).times {game.guess(@board)}
        end
        it "returns -1 as the first element of the return array" do
          expect( (game.guess(@board))[0]).to eql(-1) 
        end
        it "returns a Response filled with 0s as the second element of the return array" do
          expect( (game.guess(@board))[1]).to eql(Response.new(0,0,0,0)) 
        end
      end

      context "when guessed incorrectly on #{Game::GUESSES_ALLOWED+10}th try" do
        before do
          peg = Peg.new(4)
          @board = Board.new(peg,peg,peg,peg)
          (Game::GUESSES_ALLOWED+9).times {game.guess(@board)}
        end
        it "returns -1 as the first element of the return array" do
          expect( (game.guess(@board))[0]).to eql(-1) 
        end
        it "returns a Response filled with 0s as the second element of the return array" do
          expect( (game.guess(@board))[1]).to eql(Response.new(0,0,0,0)) 
        end
      end






      context "when guessed correctly on first try" do
        it "returns 1 as the first element of the return array" do
          expect( game.guess(Board.new(Peg.new(2), Peg.new(2), Peg.new(2), Peg.new(2)))[0] ).to eql(1)
        end
        it "returns a Response filled with 2s as the second element of the return array" do
          expect( game.guess(Board.new(Peg.new(2), Peg.new(2), Peg.new(2), Peg.new(2)))[1] ).to eql(Response.new(2,2,2,2))
        end
      end  

      context "when guessed correctly on #{Game::GUESSES_ALLOWED-1}th try" do
        before do
          peg_1 = Peg.new(2)
          peg_2 = Peg.new(4)
          @incorrect_board = Board.new(peg_2,peg_2,peg_2,peg_2)
          @correct_board = Board.new(peg_1,peg_1,peg_1,peg_1)
          (Game::GUESSES_ALLOWED-2).times {game.guess(@incorrect_board)}
        end
        it "returns 1 as the first element of the return array" do
          expect( (game.guess(@correct_board))[0]).to eql(1) 
        end
        it "returns a Response filled with 2s as the second element of the return array" do
          expect( (game.guess(@correct_board))[1]).to eql(Response.new(2,2,2,2)) 
        end
      end

     context "when guessed correctly on #{Game::GUESSES_ALLOWED}th try" do
        before do
          peg_1 = Peg.new(2)
          peg_2 = Peg.new(4)
          @incorrect_board = Board.new(peg_2,peg_2,peg_2,peg_2)
          @correct_board = Board.new(peg_1,peg_1,peg_1,peg_1)
          (Game::GUESSES_ALLOWED-1).times {game.guess(@incorrect_board)}
        end
        it "returns 1 as the first element of the return array" do
          expect( (game.guess(@correct_board))[0]).to eql(1) 
        end
        it "returns a Response filled with 2s as the second element of the return array" do
          expect( (game.guess(@correct_board))[1]).to eql(Response.new(2,2,2,2)) 
        end
      end

     context "when guessed correctly on #{Game::GUESSES_ALLOWED+1}th try" do
        before do
          peg_1 = Peg.new(2)
          peg_2 = Peg.new(4)
          @incorrect_board = Board.new(peg_2,peg_2,peg_2,peg_2)
          @correct_board = Board.new(peg_1,peg_1,peg_1,peg_1)
          (Game::GUESSES_ALLOWED).times {game.guess(@incorrect_board)}
        end
        it "returns 1 as the first element of the return array" do
          expect( (game.guess(@correct_board))[0]).to eql(-1) 
        end
        it "returns a Response filled with 2s as the second element of the return array" do
          expect( (game.guess(@correct_board))[1]).to eql(Response.new(0,0,0,0)) 
        end
      end

     context "when guessed correctly on #{Game::GUESSES_ALLOWED+10}th try" do
        before do
          peg_1 = Peg.new(2)
          peg_2 = Peg.new(4)
          @incorrect_board = Board.new(peg_2,peg_2,peg_2,peg_2)
          @correct_board = Board.new(peg_1,peg_1,peg_1,peg_1)
          (Game::GUESSES_ALLOWED+9).times {game.guess(@incorrect_board)}
        end
        it "returns 1 as the first element of the return array" do
          expect( (game.guess(@correct_board))[0]).to eql(-1) 
        end
        it "returns a Response filled with 2s as the second element of the return array" do
          expect( (game.guess(@correct_board))[1]).to eql(Response.new(0,0,0,0)) 
        end
      end



    end # guess

  end # initialize with '2,2,2,2'
end # Game
