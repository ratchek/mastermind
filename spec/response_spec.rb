require_relative "../lib/response"

describe Response do
  context "when initialized with 0,2,2,1 " do
    response =  Response.new(0,2,2,1)
    describe "at " do
      context "1" do
        it "returns 0" do
          expect(response.at(1)).to eql(0)
        end
      end

      context "2" do
        it "returns 2" do
          expect(response.at(2)).to eql(2)
        end
      end

      context "3" do
        it "returns 2" do
          expect(response.at(3)).to eql(2)
        end
      end

      context "4" do
        it "returns 1" do
          expect(response.at(4)).to eql(1)
        end
      end

      context "5" do
        it "throws an ArgumentError" do
          expect{response.at(5)}.to raise_error ArgumentError
        end
      end

      context "0" do
        it "throws an ArgumentError" do
          expect{response.at(0)}.to raise_error ArgumentError
        end
      end

      context "'c'" do
        it "throws an ArgumentError" do
          expect{response.at("c")}.to raise_error ArgumentError
        end
      end

      context "'2'" do
        it "throws an ArgumentError" do
          expect{response.at("2")}.to raise_error ArgumentError
        end
      end
     end # at

    describe ".set " do
      context "1 to 1" do
        it "sets 1st spot to 1" do
          response.set(1,1)
          expect(response.at(1)).to eql(1)
        end
      end

      context "2 to 0" do
        it "sets 2nd spot to 0" do
          response.set(2,0)
          expect(response.at(2)).to eql(0)
        end
      end

      context "3 to 2" do
        it "sets 3rd spot to 2" do
          response.set(3,2)
          expect(response.at(3)).to eql(2)
        end
      end

      context "4 to 2" do
        it "sets 4th spot to 2" do
          response.set(4,2)
          expect(response.at(4)).to eql(2)
        end
      end

      context "5 to 2" do
        it "throws an ArgumentError" do
          expect{response.set(5,2)}.to raise_error ArgumentError, "only indices that are permited are 1,2,3, or 4"
        end
      end

      context "0 to 2" do
        it "throws an ArgumentError" do
          expect{response.set(0,2)}.to raise_error ArgumentError, "only indices that are permited are 1,2,3, or 4"
        end
      end

      context "'c' to 2" do
        it "throws an ArgumentError" do
          expect{response.set('c',2)}.to raise_error ArgumentError, "only indices that are permited are 1,2,3, or 4"
        end
      end

      context "'2' to 2" do
        it "throws an ArgumentError" do
          expect{response.set('2',2)}.to raise_error ArgumentError, "only indices that are permited are 1,2,3, or 4"
        end
      end

      context "2 to 5" do
        it "throws an ArgumentError" do
          expect{response.set(2,5)}.to raise_error ArgumentError, "only values that are permited are 0, 1, or 2"
        end
      end

      context "2 to -5" do
        it "throws an ArgumentError" do
          expect{response.set(2,-5)}.to raise_error ArgumentError, "only values that are permited are 0, 1, or 2"
        end
      end

      context "2 to 'c'" do
        it "throws an ArgumentError" do
          expect{response.set(2,'c')}.to raise_error ArgumentError, "only values that are permited are 0, 1, or 2"
        end
      end

     end # set   
  end # context -  initialized 

	describe ".eql" do
    context "comparing two different Responses with equal values" do
      it "returns true" do
        response_1 = Response.new(2,1,2,0)
        response_2 = Response.new(2,1,2,0)
        expect(response_1).to eql(response_2)
      end
    end

    context "comparing two different Responses with different values" do
      it "returns false" do
        response_1 = Response.new(2,1,2,0)
        response_2 = Response.new(2,1,0,2)
        expect(response_1).not_to eql(response_2)
      end
    end
  end # .eql?

  describe ".==" do
    context "comparing two different Responses with equal values" do
      it "returns true" do
        response_1 = Response.new(1,1,1,1)
        response_2 = Response.new(1,1,1,1)
        expect(response_1==response_2).to be true
      end
    end

    context "comparing two different Responses with different values" do
      it "returns false" do
        response_1 = Response.new(1,1,1,1)
        response_2 = Response.new(0,1,1,1)
        expect(response_1==response_2).to be false
      end
    end
  end # .==


  describe ".hash" do
    context "comparing hashes of two different Responses with equal values" do
      it "returns true" do
        response_1 = Response.new(1,2,2,1)
        response_2 = Response.new(1,2,2,1)
        expect(response_1.hash == response_2.hash).to be true
      end
    end

    context "comparing hashes of two different Responses with different values" do
      it "returns false" do
        response_1 = Response.new(0,1,2,1)
        response_2 = Response.new(1,0,1,2)
        expect(response_1.hash == response_2.hash).to be false
      end
    end
  end # .hash


	context "when initialized with 3 numbers and character 'd'" do
      it "throws an argument error" do
        expect{Response.new(1,2,1,'d')}.to raise_error ArgumentError
      end
  end

  context "when initialized with one of the numbers being larger than 2" do
      it "throws an argument error" do
        expect{Response.new(1,2,1,5)}.to raise_error ArgumentError
      end
  end

  context "when initialized with one of the numbers being smaller than 0" do
      it "throws an argument error" do
        expect{Response.new(-1,2,1,0)}.to raise_error ArgumentError
      end
  end
 
end # Response


